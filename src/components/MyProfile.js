import React, {Component} from 'react';
import './myProfile.less';

class MyProfile extends Component {

  constructor(props) {
    super(props);
    this.state = {
      form: {
        name: '',
        gender: 'male',
        description: '',
        read: true
      }
    }
  }

  handleSubmit = event => {
    event.preventDefault()
    console.log(this.state.form);
  }

  handleNameChange = event => {    
    this.setState({
      form: {
        ...this.state.form,
        name: event.target.value
      }
    })
  }

  handleGenderChange = event => {    
    this.setState({
      form: {
        ...this.state.form,
        gender: event.target.value
      }
    })
  }

  handleDescriptionChange = event => {    
    this.setState({
      form: {
        ...this.state.form,
        description: event.target.value
      }
    })
  }

  handleReadChange = () => {
    this.setState({
      form: {
        ...this.state.form,
        read: !this.state.form.read
      }
    })
  }

  render() {
    const {
      state: {
        form
      },
      handleNameChange,
      handleGenderChange,
      handleDescriptionChange,
      handleReadChange,
      handleSubmit
    } = this

    return (
      <form onSubmit={handleSubmit}>
        <h1>My Profile</h1>
        <div className='form-field-item'>
          <label className='legend'>Name</label>
          <input value={form.name} onChange={handleNameChange} placeholder='Your name' />
        </div>

        <div className='form-field-item'>
          <label className='legend'>Gender</label>
          <select value={form.gender} onChange={handleGenderChange}>
            <option value='male'>Male</option>
            <option value='female'>Female</option>
          </select>
        </div>

        <div className='form-field-item'>
          <label className='legend'>Description</label>
          <textarea onChange={handleDescriptionChange} value={form.description} placeholder='Description about yourself'></textarea>
        </div>

        <div className='form-field-item'>
          <input type='checkbox' checked={form.read} onChange={handleReadChange} />
          <label>I have read the terms of product</label>
        </div>

        <input type='submit' value='Submit' />
      </form>
    );
  }
}

export default MyProfile;
